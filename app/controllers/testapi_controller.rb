class TestapiController < ApplicationController
	def testapp
	    require 'json'
	    require 'net/http'
	    s = Net::HTTP.get_response(URI.parse('http://localhost:3333/examples')).body
	    json = Hash.from_xml(s)
	    hashes = []
	    json["examples"].each do |obj|
    	  mappings = {"id"=> "id", "name" => "name", "param_1" => "some_string", "param_2" => "some_date", "some_code" => "iso_code", "some_price_param" => "price"}
    	  obj = Hash[obj.map {|k, v| [mappings[k], v] }]
    	  hashes << obj
	    end
	    hash = Hash.new 
	    hash["examples"] = hashes
	    render json: hash
  	end
end
