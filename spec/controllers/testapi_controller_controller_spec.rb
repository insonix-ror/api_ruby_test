require 'rails_helper'
RSpec.describe TestapiController, type: :controller do
let(:json) { JSON.parse(response.body) }
    describe "GET /testapp.json" do
      before do
      get :testapp, format: :json

      end 

       context 'json valid' do
        it 'returns example json' do
         @example = [{"id"=>1, "name"=>"a", "some_string"=>"a", "some_date"=>"2013-05-20T00:00:00.000Z", "iso_code"=>"b", "price"=>1.1}, {"id"=>2, "name"=>"name", "some_string"=>"param_1", "some_date"=>"2014-05-20T00:00:00.000Z", "iso_code"=>"ABC", "price"=>1.1}]
         expect(json["examples"]).to eq(@example)
        end
      end 

      context 'header test' do
        it 'return hearder content-type' do
          response.header['Content-Type'].should include 'application/json'
        end
      end  

      context 'json hash values' do
        it 'returns valid values for hashes' do
         @keys= ["id", "name", "some_string", "some_date", "iso_code", "price"]
         @h1value = [1, "a", "a", "2013-05-20T00:00:00.000Z", "b", 1.1]
         @h2value = [2, "name", "param_1", "2014-05-20T00:00:00.000Z", "ABC",1.1]
          (0..5).each do |i|
          expect(json["examples"][0][@keys[i]]).to eq(@h1value[i])
          expect(json["examples"][1][@keys[i]]).to eq(@h2value[i])
         end
        end
      end       

      context 'json replaced keys' do
        it 'returns valid json' do
         expect(((json["examples"][0].keys).third).to_s).to eq("some_string")
         expect(((json["examples"][0].keys).fourth).to_s).to eq("some_date")
         expect(((json["examples"][0].keys).fifth).to_s).to eq("iso_code")
         expect(((json["examples"][0].keys).last).to_s).to eq("price")
        end
      end    

    end



end

